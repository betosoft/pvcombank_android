package com.pvcombank.adapter;

import com.pvcombank.fragment.CategoryFragment;
import com.pvcombank.fragment.DealFragment;
import com.pvcombank.fragment.MyCardFragment;
import com.pvcombank.fragment.MyOrderFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;

public class AdapterDealsPager extends FragmentPagerAdapter{

	 private static final String[] CONTENT = new String[] { "Danh mục", "Mới nhất", "Hot nhất" };
	private Fragment mFragment;
	
	public AdapterDealsPager(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 3;
	}
	
	@Override
	public Fragment getItem(int arg0) {
		switch (arg0) {
		case 0:
			mFragment = new CategoryFragment();
			break;
		case 1:
			mFragment = new MyCardFragment();
			break;
		case 2:
			mFragment = new MyOrderFragment();
			break;
		default:
			mFragment = new MyCardFragment();
			break;
		}
		return mFragment;
	}
	
	@Override
	public int getItemPosition(Object object) {
		// TODO Auto-generated method stub
		return POSITION_NONE;
	}
	

    @Override
    public CharSequence getPageTitle(int position) {
        return CONTENT[position];
    }

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// TODO Auto-generated method stub

		FragmentManager manager = ((Fragment) object).getFragmentManager();
		FragmentTransaction trans = manager.beginTransaction();
		trans.remove((Fragment) object);
		trans.commit();
		super.destroyItem(container, position, object);

	}

}
