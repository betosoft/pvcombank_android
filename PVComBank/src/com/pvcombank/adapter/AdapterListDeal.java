package com.pvcombank.adapter;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.pvcombank.model.Deal;
import com.pvcombank.pvcombank.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class AdapterListDeal extends BaseAdapter{

	private ArrayList<Deal> lstDeal;
	private Context mContext;
	private ImageLoader iml;
	
	public AdapterListDeal(ArrayList<Deal> arr, Context context){
		this.lstDeal = arr;
		this.mContext = context;
		iml = ImageLoader.getInstance();
		ImageLoaderConfiguration config = ImageLoaderConfiguration.createDefault(mContext);
		iml.init(config);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lstDeal.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return lstDeal.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return lstDeal.indexOf(arg0);
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		View view = arg1;
		ViewHolder holder = new ViewHolder();
		if(view == null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.adapter_list_deal, arg2, false);
			holder.imgDeal = (ImageView) view.findViewById(R.id.image_deal);
			view.setTag(holder);
		}else {
			holder = (ViewHolder) view.getTag();
		}
		iml.displayImage(lstDeal.get(arg0).getImage(), holder.imgDeal, getOption());
		return view;
	}

	class ViewHolder{
		ImageView imgDeal;
	}
	
	private DisplayImageOptions getOption() {
		return new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.ic_launcher)
				.showImageOnFail(R.drawable.ic_launcher)
				.bitmapConfig(Bitmap.Config.RGB_565)
				// .imageScaleType(ImageScaleType.EXACTLY)
				.cacheInMemory(true).considerExifParams(true)
				.build();
	}
}
