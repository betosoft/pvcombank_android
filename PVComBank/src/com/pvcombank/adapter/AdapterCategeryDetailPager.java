package com.pvcombank.adapter;

import com.pvcombank.fragment.CategoryDetailFragment;
import com.pvcombank.fragment.CategoryFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;

public class AdapterCategeryDetailPager extends FragmentPagerAdapter {

	private String category = "0";

	public AdapterCategeryDetailPager(FragmentManager fm, String category) {
		super(fm);
		this.category = category;
		// TODO Auto-generated constructor stub
	}

	private final String[] TITLE_TAP = { "Mới nhất", "Hot nhất", "Giá rẻ",
			"Vị trí gần" };
	private Fragment mFragment;

	@Override
	public Fragment getItem(int arg0) {
		// TODO Auto-generated method stub
		Fragment fragment = new CategoryDetailFragment();
		Bundle bundle = new Bundle();
		bundle.putString("category", category);
		switch (arg0) {
		case 0:
			bundle.putString("ordering", "0");
			break;
		case 1:
			bundle.putString("ordering", "3");
			break;
		case 2:
			bundle.putString("ordering", "1");
			break;
		case 3:
			bundle.putString("ordering", "2");
			break;
		default:
			bundle.putString("ordering", "0");
			break;
		}
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 4;
	}

	@Override
	public int getItemPosition(Object object) {
		// TODO Auto-generated method stub
		return POSITION_NONE;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return TITLE_TAP[position];
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// TODO Auto-generated method stub

		FragmentManager manager = ((Fragment) object).getFragmentManager();
		FragmentTransaction trans = manager.beginTransaction();
		trans.remove((Fragment) object);
		trans.commit();
		super.destroyItem(container, position, object);

	}

}
