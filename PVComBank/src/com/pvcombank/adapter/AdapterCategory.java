package com.pvcombank.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.FIFOLimitedMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.pvcombank.model.Category;
import com.pvcombank.pvcombank.R;

public class AdapterCategory extends BaseAdapter {

	private ArrayList<Category> arr;
	private Context mContext;
	private ImageLoader iml;

	public AdapterCategory(ArrayList<Category> arr, Context context) {
		this.arr = arr;
		this.mContext = context;
		iml = ImageLoader.getInstance();
		ImageLoaderConfiguration config = ImageLoaderConfiguration.createDefault(mContext);
//				new ImageLoaderConfiguration.Builder(
//				mContext.threadPoolSize(5)
//				.memoryCache(new WeakMemoryCache())
//				.memoryCacheSize(memoryCacheSize)
//				.memoryCache(new FIFOLimitedMemoryCache(100))
//				.denyCacheImageMultipleSizesInMemory().build();
		iml.init(config);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arr.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arr.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arr.indexOf(getItem(arg0));
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		View view = arg1;
		ViewHolder holder = new ViewHolder();
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.adapter_category, arg2, false);
			holder.imgCategory = (ImageView) view
					.findViewById(R.id.image_category);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		iml.displayImage(arr.get(arg0).getImage(), holder.imgCategory, getOption());
//		Log.d("Quang Huy", "url: " + arr.get(arg0).getImage());
//		Bitmap bm = iml.loadImageSync(arr.get(arg0).getImage(), getOption());
//		bm = getRoundedCornerBitmap(bm, 20);
//		holder.imgCategory.setImageBitmap(bm);
		return view;
	}

	class ViewHolder {
		ImageView imgCategory;
	}
	
	private Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

	private DisplayImageOptions getOption() {
		return new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.ic_launcher)
				.showImageOnFail(R.drawable.ic_launcher)
				.bitmapConfig(Bitmap.Config.RGB_565)
				// .imageScaleType(ImageScaleType.EXACTLY)
				.cacheInMemory(true).considerExifParams(true)
				.build();
	}
	
}
