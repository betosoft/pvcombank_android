package com.pvcombank.pvcombank;

import java.util.HashMap;

import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.crittercism.app.Crittercism;
import com.google.gson.Gson;
import com.pvcombank.libs.ApiManager;
import com.pvcombank.libs.ApiManager.CompleteListener;
import com.pvcombank.libs.ApiResponse;
import com.pvcombank.libs.BaseModel;
import com.pvcombank.libs.DeviceInfo;
import com.pvcombank.libs.XLog;
import com.pvcombank.libs.XUtil;
import com.pvcombank.model.AppConfig;
import com.pvcombank.model.ListCategory;
import com.vpcombank.view.AlertDialogUtils;
import com.vpcombank.view.AlertDialogUtils.IOnDialogClickListener;
import com.vpcombank.view.ErrorDialogFragment;

public class LoadingActivity extends BaseActivity {

	private Button btnRetry;
	private ProgressBar progressBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crittercism.initialize(getApplicationContext(),
				"556c0ac667a3707e4fe35e76");
		setContentView(R.layout.activity_loading);
		btnRetry = (Button) findViewById(R.id.btn_retry);
		progressBar = (ProgressBar) findViewById(R.id.progress_bar);
		btnRetry.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				progressBar.setVisibility(View.VISIBLE);
				btnRetry.setVisibility(View.GONE);
				initSession();
				// startMainActivity();
			}
		});

		if (XUtil.hasNetworkConnection(this)) {
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					initSession();
				}
			}, 200);

		} else {
			String sessionId = App.getInstance().getSessionId();
			if (sessionId != null && sessionId.length() > 0) {
				Intent mainIntent = new Intent(this, MainActivity.class);
				mainIntent.putExtra("is_in_offline_mode", true);
				startActivity(mainIntent);
				finish();
			} else {
				btnRetry.setVisibility(View.VISIBLE);

			}
		}

	}

	private void initSession() {
		if (App.getInstance().getSessionId() == null) {
			HashMap<String, String> params = new HashMap<String, String>();
			params.put("uuid",
					DeviceInfo.getDeviceIdentity(LoadingActivity.this));
			params.put("device_name", DeviceInfo.getDeviceModel());
			params.put("device_model", DeviceInfo.getDeviceModel());
			params.put("os_version", DeviceInfo.getOsVersion());
			Point screenDemension = DeviceInfo.getScreenDemension(this);
			params.put("screen_width", Integer.toString(screenDemension.x));
			params.put("screen_height", Integer.toString(screenDemension.y));
			params.put("screen_density", Integer.toString(DeviceInfo
					.getDensity(LoadingActivity.this)));
			if (XUtil.hasNetworkConnection(LoadingActivity.this)) {
				ApiManager.post(Constant.API_INIT_SESSION, params,
						new CompleteListener() {

							@Override
							public void onResponse(ApiResponse responseData) {
								String sessionId = null;
								if (!responseData.isError()) {
									sessionId = responseData.getData()
											.optString("session_id", null);
									Log.d("Quang Huy", "session_id: " + sessionId);
								} else {
									btnRetry.setVisibility(View.VISIBLE);
								}

								if (responseData.getCode() == Constant.API_ERROR_CODE_UPDATE_ERROR) {
									showUpadate(responseData.getMessage(),
											responseData.getUpdateLink(), false);
								} else {
									if (sessionId == null
											|| sessionId.length() < 5) {
										// error
										XLog.e("get session id failed: "
												+ responseData.getMessage());
										// display retry
										btnRetry.setVisibility(View.VISIBLE);
									} else {
										App.getInstance().setSessionId(
												sessionId);
										loadAppConfig();
									}
								}
							}
						});
			}
		} else {
			loadAppConfig();
		}
	}

	//
	// private void showRetryButton() {
	// btnRetry.setVisibility(View.VISIBLE);
	// progressBar.setVisibility(View.GONE);
	// }

	private void loadAppConfig() {
		ApiManager.post(Constant.API_LOAD_CONFIG,
				new ApiManager.CompleteListener() {

					@Override
					public void onResponse(ApiResponse responseData) {
						if (responseData.isError()) {
							if (responseData.getCode() == Constant.API_ERROR_CODE_UPDATE_ERROR) {
								try {
									showUpadate(responseData.getMessage(),
											responseData.getUpdateLink(), false);
								} catch (Exception e) {
									e.printStackTrace();
								}

							} else {
								btnRetry.setVisibility(View.VISIBLE);
								if (responseData.getCode() == Constant.API_ERROR_CODE_SESSION_TIMEOUT) {
									App.getInstance().setSessionId("");
								} else {
									if (isLive) {
										ErrorDialogFragment
												.newInstance(
														getString(R.string.connect_to_server_error))
												.show(getSupportFragmentManager(),
														"appconfig_error");
									}

								}
							}

						} else {
							
							Gson gson = BaseModel.newGson();
							AppConfig appConfig = gson.fromJson(responseData
									.getData().toString(), AppConfig.class);
							App.getInstance().setAppConfig(
									LoadingActivity.this, appConfig);
							
							if (appConfig.getUpdate() == 1) {
								showUpadate(appConfig.getUpdateMessage(),
										appConfig.getUpdateLink(), true);
							} else {
								startMainActivity();
							}

						}
					}
				});
	}

	private void showUpadate(String message, final String url,
			boolean hasNegativeBt) {
		AlertDialogUtils.showInfoDialogUpdate(LoadingActivity.this, message,
				hasNegativeBt, new IOnDialogClickListener() {

					@Override
					public void onClickOk() {
						Intent browserIntent = new Intent(Intent.ACTION_VIEW,
								Uri.parse(url));
						startActivity(browserIntent);
					}

					@Override
					public void onClickCancel() {
						startMainActivity();
					}
				});
	}

	private void startMainActivity() {
		if (App.getInstance().getUser() != null) {
			Intent mainIntent = new Intent(this, MainActivity.class);
			startActivity(mainIntent);
		} else {
			Intent intent = new Intent(this, LoginFacebookActivity.class);
			startActivity(intent);
		}
		finish();
	}
	
}
