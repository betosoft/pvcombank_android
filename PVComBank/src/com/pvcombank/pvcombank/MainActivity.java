package com.pvcombank.pvcombank;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.gc.materialdesign.views.LayoutRipple;
import com.pvcombank.fragment.DealFragment;

public class MainActivity extends BaseActivity implements OnClickListener {

	private LayoutRipple layoutDeals;
	private LayoutRipple layoutAround;
	private LayoutRipple layoutSearch;
	private LayoutRipple layoutOrder;
	private LayoutRipple layoutMore;
	private ImageView imgDeals;
	private ImageView imgAround;
	private ImageView imgSearch;
	private ImageView imgOrder;
	private ImageView imgMore;
	private TextView tvDeals;
	private TextView tvAround;
	private TextView tvSearch;
	private TextView tvOrder;
	private TextView tvMore;

	private final int colorWhite = Color.parseColor("#FFFFFF");
	private final int colorTextDefault = Color.parseColor("#9f6c16");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initUI();
		initControl();
	}

	private void initUI() {
		layoutDeals = (LayoutRipple) findViewById(R.id.layout_deals);
		layoutAround = (LayoutRipple) findViewById(R.id.layout_around);
		layoutSearch = (LayoutRipple) findViewById(R.id.layout_search);
		layoutOrder = (LayoutRipple) findViewById(R.id.layout_order);
		layoutMore = (LayoutRipple) findViewById(R.id.layout_more);
		imgDeals = (ImageView) findViewById(R.id.img_deals);
		imgAround = (ImageView) findViewById(R.id.img_around);
		imgSearch = (ImageView) findViewById(R.id.img_search);
		imgOrder = (ImageView) findViewById(R.id.img_order);
		imgMore = (ImageView) findViewById(R.id.img_more);
		tvDeals = (TextView) findViewById(R.id.txt_deals);
		tvAround = (TextView) findViewById(R.id.txt_around);
		tvSearch = (TextView) findViewById(R.id.txt_search);
		tvOrder = (TextView) findViewById(R.id.txt_order);
		tvMore = (TextView) findViewById(R.id.txt_more);
	}

	private void initControl() {
		layoutDeals.setOnClickListener(this);
		layoutAround.setOnClickListener(this);
		layoutSearch.setOnClickListener(this);
		layoutOrder.setOnClickListener(this);
		layoutMore.setOnClickListener(this);
	}

	/**
	 * update giao dien cho bottom bar
	 * 
	 * @param pos
	 */
	private void updateBottomBar(int pos) {
		switch (pos) {
		case 0:
			imgDeals.setImageResource(R.drawable.ic_launcher);
			imgAround.setImageResource(R.drawable.ic_launcher);
			imgSearch.setImageResource(R.drawable.ic_launcher);
			imgOrder.setImageResource(R.drawable.ic_launcher);
			imgMore.setImageResource(R.drawable.ic_launcher);
			tvDeals.setTextColor(colorWhite);
			tvAround.setTextColor(colorTextDefault);
			tvSearch.setTextColor(colorTextDefault);
			tvOrder.setTextColor(colorTextDefault);
			tvMore.setTextColor(colorTextDefault);
			break;
		case 1:
			imgDeals.setImageResource(R.drawable.ic_launcher);
			imgAround.setImageResource(R.drawable.ic_launcher);
			imgSearch.setImageResource(R.drawable.ic_launcher);
			imgOrder.setImageResource(R.drawable.ic_launcher);
			imgMore.setImageResource(R.drawable.ic_launcher);
			tvDeals.setTextColor(colorTextDefault);
			tvAround.setTextColor(colorWhite);
			tvSearch.setTextColor(colorTextDefault);
			tvOrder.setTextColor(colorTextDefault);
			tvMore.setTextColor(colorTextDefault);
			break;
		case 2:
			imgDeals.setImageResource(R.drawable.ic_launcher);
			imgAround.setImageResource(R.drawable.ic_launcher);
			imgSearch.setImageResource(R.drawable.ic_launcher);
			imgOrder.setImageResource(R.drawable.ic_launcher);
			imgMore.setImageResource(R.drawable.ic_launcher);
			tvDeals.setTextColor(colorTextDefault);
			tvAround.setTextColor(colorTextDefault);
			tvSearch.setTextColor(colorWhite);
			tvOrder.setTextColor(colorTextDefault);
			tvMore.setTextColor(colorTextDefault);
			break;
		case 3:
			imgDeals.setImageResource(R.drawable.ic_launcher);
			imgAround.setImageResource(R.drawable.ic_launcher);
			imgSearch.setImageResource(R.drawable.ic_launcher);
			imgOrder.setImageResource(R.drawable.ic_launcher);
			imgMore.setImageResource(R.drawable.ic_launcher);
			tvDeals.setTextColor(colorTextDefault);
			tvAround.setTextColor(colorTextDefault);
			tvSearch.setTextColor(colorTextDefault);
			tvOrder.setTextColor(colorWhite);
			tvMore.setTextColor(colorTextDefault);
			break;
		case 4:
			imgDeals.setImageResource(R.drawable.ic_launcher);
			imgAround.setImageResource(R.drawable.ic_launcher);
			imgSearch.setImageResource(R.drawable.ic_launcher);
			imgOrder.setImageResource(R.drawable.ic_launcher);
			imgMore.setImageResource(R.drawable.ic_launcher);
			tvDeals.setTextColor(colorTextDefault);
			tvAround.setTextColor(colorTextDefault);
			tvSearch.setTextColor(colorTextDefault);
			tvOrder.setTextColor(colorTextDefault);
			tvMore.setTextColor(colorWhite);
			break;
		default:
			imgDeals.setImageResource(R.drawable.ic_launcher);
			imgAround.setImageResource(R.drawable.ic_launcher);
			imgSearch.setImageResource(R.drawable.ic_launcher);
			imgOrder.setImageResource(R.drawable.ic_launcher);
			imgMore.setImageResource(R.drawable.ic_launcher);
			tvDeals.setTextColor(colorWhite);
			tvAround.setTextColor(colorTextDefault);
			tvSearch.setTextColor(colorTextDefault);
			tvOrder.setTextColor(colorTextDefault);
			tvMore.setTextColor(colorTextDefault);
			break;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Fragment fragment = null;
		switch (v.getId()) {
		case R.id.layout_deals:
			fragment = new DealFragment();
			updateBottomBar(0);
			break;
		case R.id.layout_around:
			updateBottomBar(1);
			break;
		case R.id.layout_search:
			updateBottomBar(2);
			break;
		case R.id.layout_order:
			updateBottomBar(3);
			break;
		case R.id.layout_more:
			updateBottomBar(4);
			break;
		default:
			break;
		}
		replaceFragment(fragment);
	}

	private void replaceFragment(Fragment fragment) {
		if (fragment != null) {
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.frame_container, fragment).commit();
			// update selected item and title, then close the drawer
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}
}
