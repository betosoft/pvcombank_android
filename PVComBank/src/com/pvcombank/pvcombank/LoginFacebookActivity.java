package com.pvcombank.pvcombank;

import com.viewpagerindicator.CirclePageIndicator;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class LoginFacebookActivity extends BaseActivity implements
		android.view.View.OnClickListener {
	private TextView tvLoginFB;
	private CirclePageIndicator mIndicator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_fb);
		setFind();
		setControl();

	}

	private void setFind() {
		tvLoginFB = (TextView) findViewById(R.id.tv_login_facebook);
		tvLoginFB.setOnClickListener(this);
	}

	private void setControl() {
	}

	@Override
	protected void onFacebookLoginSuccess() {
		// TODO Auto-generated method stub
		super.onFacebookLoginSuccess();
		startActivity(new Intent(this, MainActivity.class));
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {
		case R.id.tv_login_facebook:
			isLogin = true;
			mFacebookUtil.login(this);
			break;
		default:
			break;
		}
	}

}
