package com.pvcombank.pvcombank;

public final class Constant {
	public static char[] API_KEY = new char[] { '2', '0', '0', '5', '6', '0',
			'1', '0', '6', '9', '4' };

	// api error codes
	public static final int API_ERROR_CODE_BAD_REQUEST = 400;
	public static final int API_ERROR_CODE_API_NOT_EXIST = 404;
	public static final int API_ERROR_CODE_UNAUTHORIZED = 403;
	public static final int API_ERROR_CODE_UPDATE_ERROR = 410;
	public static final int API_ERROR_CODE_SESSION_TIMEOUT = 402; // 440
	public static final int API_ERROR_CODE_SERVER_ERROR = 500;
	public static final int API_ERROR_CODE_SERVER_OFFLINE = 503;
	public static final int API_ERROR_CODE_UNKNOW = 9999;
	public static final String API_RESPONSE_ERROR_KEY_NAME = "error";
	public static final String API_RESPONSE_ERROR_CODE_NAME = "code";
	public static final String API_RESPONSE_ERROR_MESSAGE_NAME = "message";
	// ////////////////////////////////////////////////////
	// 123clip
	public static final String FOLDER_IMAGE = "images";
	public static final String FOLDER_MUSIC = "cache";
	public static final String FOLDER_LOGS = "logs";
	public static final String MUSIC_EXT = ".dms.mp3";
	public static final String FOLDER_ROOT = "chapter";
	// ///////////
	public static final String API_ROOT = "http://api.pvcomb.givestore.net";

	public static final String EXTRA_MESSAGE = "message";
	public static final String GCM_SENDER_ID = "292524911875";

	// //App
	public static final String API_INIT_SESSION = API_ROOT
			+ "/app/init_session";
	public static final String API_LOAD_CONFIG = API_ROOT + "/app/cfg";
	public static final String API_HELP = API_ROOT + "/app/help";
	public static final String API_CONTACT = API_ROOT + "/app/contact";

	// notification
	public static final String API_NOTIFICATION_REGISTER = API_ROOT
			+ "/app/register_notification";

	// ///
	public static final String API_ATM_LIST = API_ROOT + "/app/atm";
	public static final String API_CITY_LIST = API_ROOT + "/app/city";
	public static final String API_CATEGORY = API_ROOT + "/category";

	// deal
	public static final String API_DEAL_LIST = API_ROOT + "/deal";
	public static final String API_DEAL_DETAIL = API_ROOT + "/deal/details";
	public static final String API_DEAL_SEARCH = API_ROOT + "/deal/search";
	public static final String API_DEAL_FEATURED = API_ROOT + "/deal/featured";
	public static final String API_DEAL_ABC = API_ROOT + "/deal/alphabet";

	// user
	public static final String API_USER_LOGIN_FB = API_ROOT + "/user/login_fb";
	public static final String API_USER_LOGOUT = API_ROOT + "/user/loguot";
	public static final String API_USER_GET_OTHER = API_ROOT + "/user";

}