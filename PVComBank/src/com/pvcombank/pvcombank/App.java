package com.pvcombank.pvcombank;

import java.util.HashMap;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.pvcombank.libs.BaseModel;
import com.pvcombank.libs.DataStore;
import com.pvcombank.libs.GsonHelper;
import com.pvcombank.libs.ImageCacheManager;
import com.pvcombank.libs.RequestManager;
import com.pvcombank.libs.StringHelper;
import com.pvcombank.libs.XLog;
import com.pvcombank.libs.XUtil;
import com.pvcombank.model.AppConfig;
import com.pvcombank.model.User;

public class App extends Application {
	private static App instance = null;
	private String sessionId;

	public enum TrackerName {
		APP_TRACKER, // Tracker used only in this app.
		GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg:
						// roll-up tracking.
		ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a
							// company.
	}

	HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();
	private User user;
	private AppConfig appConfig;
	private boolean isAppRunning;

	@Override
	public void onCreate() {
		instance = this;
		// init DataStore
		DataStore.init(getApplicationContext());
		RequestManager.init(getApplicationContext());
		// init image cache
		ImageCacheManager.init(getApplicationContext());
		XLog.e(XUtil.getFbHashKey(getApplicationContext()));
		super.onCreate();
	}

	public static App getInstance() {
		return instance;
	}

	public String getSessionId() {
		if (sessionId == null) {
			sessionId = DataStore.getInstance().getString("session_id", "");
		}

		if (sessionId.trim().length() < 5) {
			sessionId = null;
		} else {
			// try to get user info
			String jsonUserInfo = DataStore.getInstance().getString(sessionId);
			if (jsonUserInfo != null) {
				Gson gson = new Gson();
				try {
					user = gson.fromJson(jsonUserInfo, User.class);
				} catch (Exception e) {
				}
			}
		}
		XLog.d("Session : "+ sessionId);
		return sessionId;
	}

	// private String getUUID() {
	// // do stuff here
	// DataStore dataStore = DataStore.getInstance();
	// String result = dataStore.generateRandomUUID();
	// return result;
	// }

	public User getUser() {
		if (user == null) {
			String data = DataStore.getInstance().getString("user");
			Gson gson = BaseModel.newGson();
			user = gson.fromJson(data, User.class);

		}
		return user;
	}

	public void setUser(User user) {
		this.user = user;
		DataStore.getInstance().saveString("user", user.toJson()).commit();
	}

	public void setSessionId(String sessionId) {
		if (this.sessionId != sessionId) {
			this.sessionId = sessionId;
			DataStore.getInstance().saveString("session_id", sessionId)
					.commit();
		}
	}

	public void setAppConfig(Context context, AppConfig appConfig) {
		this.appConfig = appConfig;
		Gson gson = GsonHelper.getGsonSetting();
		String data = gson.toJson(appConfig);
		DataStore.getInstance().saveString("app_config", data).commit();
	}

	public AppConfig getAppConfig(Context context) {
		if (appConfig == null) {
			Gson gson = GsonHelper.getGsonSetting();
			String data = DataStore.getInstance().getString("app_config", null);
			if (data != null && data.length() > 0) {
				appConfig = gson.fromJson(data, AppConfig.class);
			}
		}
		return this.appConfig;
	}

	private synchronized Tracker getTracker(TrackerName trackerId) {
		if (!mTrackers.containsKey(trackerId)) {

			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics
					.newTracker(R.xml.app_tracker)
					: (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics
							.newTracker(R.xml.global_tracker) : analytics
							.newTracker(R.xml.ecommerce_tracker);
			mTrackers.put(trackerId, t);

		}
		return mTrackers.get(trackerId);
	}

	public void sendSreenName(String screenName) {
		if (StringHelper.isBlank(screenName)) {
			return;
		}
		// Get tracker.
		// screenName = BaseActivity.class.getSimpleName();
		Tracker t = getTracker(TrackerName.APP_TRACKER);

		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName(screenName);

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());
		XLog.d("App tracker", "-------------> send screen name =" + screenName);
	}

	public void sendSreenName(Activity context, String screenName) {
		if (StringHelper.isBlank(screenName)) {
			return;
		}
		// Get tracker.
		Tracker t = ((App) context.getApplication())
				.getTracker(TrackerName.APP_TRACKER);

		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName(screenName);

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());
	}

	public void setAppRunnging(boolean b) {
		isAppRunning = b;
	}

	public boolean isAppRunning() {
		return isAppRunning;
	}

	public void sendEvent(String categoryId, String actionId, String labelId) {
		sendEvent(categoryId, actionId, labelId, 0);
	}

	private void sendEvent(String categoryId, String actionId, String labelId,
			long value) {
		Tracker t = getTracker(TrackerName.APP_TRACKER);
		// Build and send an Event.
		HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder()
				.setCategory(categoryId).setAction(actionId).setLabel(labelId);
		if (value > 0) {
			builder.setValue(value);
		}
		t.send(builder.build());

		XLog.d("App tracker", "-------------> sendEvent" + categoryId + " - "
				+ actionId + " - " + labelId);
	}

	public void addDuration() {
		long currentTime = System.currentTimeMillis();
		int duration = (int) (currentTime - DataStore.getInstance().getLong(
				"start_duration", currentTime));
		if (duration > 0) {
			int lastduration = DataStore.getInstance().getInt("duration", 0);
			DataStore.getInstance()
					.saveInt("duration", lastduration + duration).commit();
		}
	}

	public int getDuration() {

		return DataStore.getInstance().getInt("duration", 0);
	}

	public void restartDuration() {
		DataStore.getInstance().saveInt("duration", 0).commit();
	}

	public void startDuration() {
		DataStore.getInstance().saveLong("start_duration",
				System.currentTimeMillis()).commit();
	}

}
