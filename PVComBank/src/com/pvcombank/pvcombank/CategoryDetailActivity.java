package com.pvcombank.pvcombank;

import com.gc.materialdesign.views.LayoutRipple;
import com.pvcombank.adapter.AdapterCategeryDetailPager;
import com.pvcombank.adapter.AdapterCategory;
import com.viewpagerindicator.TabPageIndicator;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class CategoryDetailActivity extends FragmentActivity implements
		OnClickListener {

	private LayoutRipple layoutBack;
	private LayoutRipple layoutSearch;
	private TextView tvLableCategory;
	private TabPageIndicator indicator;
	private ViewPager pagerCategoryDetail;
	private AdapterCategeryDetailPager adapter;
	
	private String mCategoryId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category_detail);
		mCategoryId = getIntent().getStringExtra("category_id");
		initUI();
	}

	private void initUI() {
		layoutBack = (LayoutRipple) findViewById(R.id.layout_back);
		layoutSearch = (LayoutRipple) findViewById(R.id.layout_search);
		tvLableCategory = (TextView) findViewById(R.id.txt_title_category);
		indicator = (TabPageIndicator) findViewById(R.id.indicator);
		pagerCategoryDetail = (ViewPager) findViewById(R.id.pager_category_detail);
		adapter = new AdapterCategeryDetailPager(getSupportFragmentManager(),
				mCategoryId);
		pagerCategoryDetail.setAdapter(adapter);
		indicator.setViewPager(pagerCategoryDetail);
	}

	private void initControl() {

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub

	}

}
