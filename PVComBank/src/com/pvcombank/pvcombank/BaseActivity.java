package com.pvcombank.pvcombank;

import java.util.HashMap;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.view.Window;

import com.facebook.Session;
import com.facebook.SessionState;
import com.google.gson.Gson;
import com.pvcombank.libs.ApiManager;
import com.pvcombank.libs.ApiManager.CompleteListener;
import com.pvcombank.libs.ApiResponse;
import com.pvcombank.libs.GsonHelper;
import com.pvcombank.libs.XLog;
import com.pvcombank.model.User;
import com.vpcombank.facebook.FacebookStatusCallback;
import com.vpcombank.facebook.FacebookUtil;
import com.vpcombank.view.ProgressDialogFragment;

public class BaseActivity extends FragmentActivity implements
		FacebookStatusCallback {
	protected static final String TAG = BaseActivity.class.getSimpleName();
	public boolean isLoading;
	protected ProgressDialogFragment dlgLoading;
	protected boolean isLive;
	private boolean isSessionTimeoutReloaded;
	protected FacebookUtil mFacebookUtil;
	protected boolean isLogin = false;
	private boolean isShare = false;

	BroadcastReceiver exitApp = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			finish();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		if (savedInstanceState != null
				&& savedInstanceState.containsKey("dialog_loading")) {
			dlgLoading = (ProgressDialogFragment) getSupportFragmentManager()
					.getFragment(savedInstanceState, "dialog_loading");

		}
		mFacebookUtil = FacebookUtil.newInstance(this);
		mFacebookUtil.onCreate(savedInstanceState);
		mFacebookUtil.setFacebookStatusCallback(this);

	}

	public String getScreenName() {
		return null;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			break;

		default:
			break;
		}

		return true;

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		isLive = false;
		super.onPause();
	}

	@Override
	protected void onResume() {
		isLive = true;
		super.onResume();
	}

	public void showLoading() {
		isLoading = true;
		if (dlgLoading == null) {
			dlgLoading = (ProgressDialogFragment) getSupportFragmentManager()
					.findFragmentByTag("dialog_loading");
		}

		if (dlgLoading == null) {
			dlgLoading = ProgressDialogFragment
					.newInstance(getString(R.string.loading_please_wait));
		}

		try {
			getSupportFragmentManager().beginTransaction().remove(dlgLoading)
					.commitAllowingStateLoss();
		} catch (Exception e) {
			XLog.e("test", "remove loading dialog error: " + e.getMessage());
		}
		dlgLoading.setCancelable(false);
		dlgLoading.show(getSupportFragmentManager(), "dialog_loading");
	}

	public void hideLoading() {
		isLoading = false;
		if (dlgLoading != null && dlgLoading.isAdded()
				&& dlgLoading.isResumed()) {
			dlgLoading.dismiss();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mFacebookUtil.onSaveInstanceState(outState);
		if (dlgLoading != null && dlgLoading.isAdded()) {
			getSupportFragmentManager().putFragment(outState, "dialog_loading",
					dlgLoading);
		}
	}

	protected void handleApiError(ApiResponse responseData) {
		if (responseData.isError()) {
			if (!isSessionTimeoutReloaded
					&& responseData.getCode() == Constant.API_ERROR_CODE_SESSION_TIMEOUT) {
				isSessionTimeoutReloaded = true;
				finish();
				App.getInstance().setSessionId(null);
				Intent loading = new Intent(this, LoadingActivity.class);
				startActivity(loading);
			}
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		// TODO Auto-generated method stub
		super.onActivityResult(arg0, arg1, arg2);
		mFacebookUtil.onActivityResult(this, arg0, arg1, arg2);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		mFacebookUtil.onStart();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		mFacebookUtil.onStop();
	}

	protected void facebookLogin() {

	};

	protected void onFacebookLoginSuccess() {

	};

	protected void onFacebookLoginFail() {

	};

	@Override
	public void facebookStatusCallback(Session session, SessionState state,
			Exception exception) {
		// TODO Auto-generated method stub
		if (session.isOpened() && isLogin) {
			isLogin = false;
			loginFacebook(session.getAccessToken());
			showLoading();
			if (isShare) {
				isShare = false;
				// shareFacebook();
			}
		}

	}

	protected void loginFacebook(String token) {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("token", token);
		ApiManager.post(Constant.API_USER_LOGIN_FB, params,
				new CompleteListener() {

					@Override
					public void onResponse(ApiResponse responseData) {
						// TODO Auto-generated method stub
						hideLoading();
						if (!responseData.isError()) {
							Gson gson = GsonHelper.getGsonSetting();
							User user = gson.fromJson(responseData.getData()
									.toString(), User.class);
							App.getInstance().setUser(user);
							onFacebookLoginSuccess();
						} else {
							onFacebookLoginFail();
							handleApiError(responseData);
						}
					}
				});
	}

	// public void share(Book book) {
	//
	// mBook = book;
	// if (mFacebookUtil.isLoggedIn()) {
	// shareFacebook();
	// } else {
	// isShare = true;
	// isLogin = true;
	// mFacebookUtil.login(BaseActivity.this);
	// }
	// }
	//
	// private void shareFacebook() {
	// String url = mBook.getLinkShare();
	// String title = mBook.getTitle() + " - " + mBook.getAuthorName();
	//
	// if (FacebookDialog.canPresentShareDialog(getApplicationContext(),
	// FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
	// // Publish the post using the Share Dialog
	// mFacebookUtil.createShareDialogBuilder(title, "", url,
	// mBook.getCover()).build().present();
	// } else {
	// // mFacebookUtil
	// // .createWebShareDialogBuilder("Title",
	// // "description", url, imgUrl).build().show();
	// mFacebookUtil.publishFeedDialog(title, "", "", url, mBook.getCover());
	// }
	// }
	//
	// @TargetApi(Build.VERSION_CODES.KITKAT)
	// @SuppressLint({ "NewApi", "InlinedApi" })
	// protected void setColorStatus(int color) {
	// // if (Build.VERSION.SDK_INT >= 21) {
	// // Window window = this.getWindow();
	// //
	// window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
	// // window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
	// // window.setStatusBarColor(this.getResources().getColor(color));
	// // }
	// }
}
