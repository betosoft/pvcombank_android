package com.pvcombank.fragment;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.pvcombank.libs.ApiResponse;
import com.pvcombank.pvcombank.App;
import com.pvcombank.pvcombank.Constant;
import com.pvcombank.pvcombank.LoadingActivity;

public class BaseFragment extends Fragment {
	protected void hanlderTimeout(ApiResponse responseData) {
		if (responseData.getCode() == Constant.API_ERROR_CODE_SESSION_TIMEOUT) {
			App.getInstance().setSessionId(null);
			getActivity().startActivity(
					new Intent(getActivity(), LoadingActivity.class));
			getActivity().finish();
		}
	}
}
