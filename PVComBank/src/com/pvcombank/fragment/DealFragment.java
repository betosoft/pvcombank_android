package com.pvcombank.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;

import com.gc.materialdesign.views.LayoutRipple;
import com.pvcombank.adapter.AdapterDealsPager;
import com.pvcombank.pvcombank.R;
import com.viewpagerindicator.TabPageIndicator;

public class DealFragment extends Fragment {
	
	private LayoutRipple layoutSearch;
	private TabPageIndicator indicator;
	private ViewPager pagerDeals;
	private AdapterDealsPager adapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_deals, container, false);
		initUI(view);
		return view;
	}
	
	private void initUI(View view){
		layoutSearch = (LayoutRipple) view.findViewById(R.id.layout_search);
		indicator = (TabPageIndicator) view.findViewById(R.id.indicator);
		pagerDeals = (ViewPager) view.findViewById(R.id.pager_deal);
		adapter = new AdapterDealsPager(getChildFragmentManager());
		pagerDeals.setAdapter(adapter);
		indicator.setViewPager(pagerDeals);
	}
	
	private void initControl(){
		
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
	}

}
