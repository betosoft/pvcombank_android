package com.pvcombank.fragment;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.google.gson.Gson;
import com.pvcombank.adapter.AdapterCategory;
import com.pvcombank.libs.ApiManager;
import com.pvcombank.libs.ApiManager.CompleteListener;
import com.pvcombank.libs.ApiResponse;
import com.pvcombank.libs.BaseModel;
import com.pvcombank.model.Category;
import com.pvcombank.model.ListCategory;
import com.pvcombank.pvcombank.CategoryDetailActivity;
import com.pvcombank.pvcombank.Constant;
import com.pvcombank.pvcombank.R;

public class CategoryFragment extends Fragment {

	private ListView lvCategory;
	private AdapterCategory adapter;
	private ArrayList<Category> lstCategory; 
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_category, container	, false);
		initUI(view);
		getListCategory();
		initControl();
		return view;
	}
	
	private void initUI(View view){
		lstCategory = new ArrayList<Category>();
		lvCategory = (ListView) view.findViewById(R.id.lv_category);
		adapter = new AdapterCategory(lstCategory, getActivity());
		lvCategory.setAdapter(adapter);
	}
	
	private void initControl(){
		lvCategory.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent categoryDetail = new Intent(getActivity(), CategoryDetailActivity.class);
				categoryDetail.putExtra("category_id", lstCategory.get(arg2).getCategoryId());
				startActivity(categoryDetail);
			}
		});
	}
	
//	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
//        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
//                .getHeight(), Config.ARGB_8888);
//        Canvas canvas = new Canvas(output);
//
//        final int color = 0xff424242;
//        final Paint paint = new Paint();
//        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
//        final RectF rectF = new RectF(rect);
//        final float roundPx = pixels;
//
//        paint.setAntiAlias(true);
//        canvas.drawARGB(0, 0, 0, 0);
//        paint.setColor(color);
//        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
//
//        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
//        canvas.drawBitmap(bitmap, rect, rect, paint);
//
//        return output;
//    }

	private void getListCategory() {
		ApiManager.get(Constant.API_CATEGORY, new CompleteListener() {

			@Override
			public void onResponse(ApiResponse responseData) {
				// TODO Auto-generated method stub
				
				if (responseData.isError()) {
					if (responseData.getCode() == Constant.API_ERROR_CODE_UPDATE_ERROR) {

					} else {
					}
				} else {
					Gson gson = BaseModel.newGson();
					ListCategory category = gson.fromJson(responseData
							.getData().toString(), ListCategory.class);
					lstCategory = category.getData();
					adapter = new AdapterCategory(lstCategory	, getActivity());
					lvCategory.setAdapter(adapter);
				}
			}
		});
	}


}
