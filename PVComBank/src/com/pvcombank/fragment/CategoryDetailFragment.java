package com.pvcombank.fragment;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ListView;

import com.google.gson.Gson;
import com.pvcombank.adapter.AdapterListDeal;
import com.pvcombank.libs.ApiManager;
import com.pvcombank.libs.BaseModel;
import com.pvcombank.libs.GsonHelper;
import com.pvcombank.libs.ApiManager.CompleteListener;
import com.pvcombank.libs.ApiResponse;
import com.pvcombank.model.Deal;
import com.pvcombank.model.ListDeal;
import com.pvcombank.pvcombank.Constant;
import com.pvcombank.pvcombank.R;

public class CategoryDetailFragment extends Fragment {

	private ListView lvListDeal;
	private AdapterListDeal adapter;
	private ArrayList<Deal> lstDeal;

	// params
	private int limit = 10;
	private int offset = 0;
	private String ordering = "0";
	private String category;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_category_detail,
				container, false);
		ordering = getArguments().getString("ordering");
		category = getArguments().getString("category");
		initUI(view);
		getListDeal();
		return view;
	}

	private void initUI(View view) {
		lvListDeal = (ListView) view.findViewById(R.id.list_deal);
		lstDeal = new ArrayList<Deal>();
		adapter = new AdapterListDeal(lstDeal, getActivity());
		lvListDeal.setAdapter(adapter);
	}

	private void initControl() {
		
	}

	private void getListDeal() {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("limit", "" + limit);
		params.put("offset", "" + offset);
		params.put("category", category);
		params.put("ordering", ordering);
		params.put("long", "");
		params.put("lat", "");
		params.put("max_distance", "");
		ApiManager.get(Constant.API_DEAL_LIST, params, new CompleteListener() {

			@Override
			public void onResponse(ApiResponse responseData) {
				// TODO Auto-generated method stub
				if (responseData.isError()) {

				} else {
					Gson gson = BaseModel.newGson();
					ListDeal listDeal = gson.fromJson(responseData.getData()
							.toString(), ListDeal.class);
					lstDeal = listDeal.getData();
					if(lstDeal != null){
						adapter = new AdapterListDeal(lstDeal, getActivity());
						lvListDeal.setAdapter(adapter);
					}
				}
			}
		});
	}

}
