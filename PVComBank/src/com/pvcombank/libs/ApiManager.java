package com.pvcombank.libs;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.net.Uri;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pvcombank.pvcombank.App;
import com.pvcombank.pvcombank.Constant;

/**
 * API Helper class.
 * 
 * @author HungLV
 */
public class ApiManager {
	public static final String TAG = ApiManager.class.getSimpleName();
	public static final int METHOD_POST = com.android.volley.Request.Method.POST;
	public static final int METHOD_GET = com.android.volley.Request.Method.GET;
	public static final int METHOD_PUT = com.android.volley.Request.Method.PUT;
	public static final int METHOD_DELETE = com.android.volley.Request.Method.DELETE;

	private HashMap<String, String> defaultParams;

	private static ApiManager mInstance;

	public static ApiManager getInstance() {
		if (mInstance == null)
			mInstance = new ApiManager();

		return mInstance;
	}

	private ApiManager() {
		defaultParams = new HashMap<String, String>();
	}

	public static void get(String url, final CompleteListener listener) {
		get(url, null, listener, null);
	}

	public static void get(String url, HashMap<String, String> params, final CompleteListener listener) {
		get(url, params, listener, null);
	}
	
	public static void get(String url, HashMap<String, String> params, final CompleteListener listener, String tag) {
		ApiManager.getInstance().request(METHOD_GET, url, params, listener, tag);
	}

	public static void post(String url, final CompleteListener listener) {
		ApiManager.getInstance().request(METHOD_POST, url, null, listener, null);
	}

	public static void post(String url, HashMap<String, String> params, final CompleteListener listener) {
		post(url, params, listener, null);
	}
	
	public static void post(String url, HashMap<String, String> params, final CompleteListener listener, String tag) {
		ApiManager.getInstance().request(METHOD_POST, url, params, listener, tag);
	}

	public void request(final int method, String url, HashMap<String, String> params, final CompleteListener listener, String tag) {
		// adding default params
		HashMap<String, String> fullParams = new HashMap<String, String>();
		fullParams.putAll(this.defaultParams);
		if (params != null) {
			fullParams.putAll(params);
		}

		XLog.d(TAG, "request params:" + fullParams.toString());

		if (method == METHOD_GET) {
			Uri.Builder b = Uri.parse(url).buildUpon();
			for (Map.Entry<String, String> entry : fullParams.entrySet()) {
				b.appendQueryParameter(entry.getKey(), entry.getValue());
			}
			url = b.toString();
			fullParams.clear();
		}

		Listener<JSONObject> completeListener = new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				XLog.d(TAG, "RESULT: " + response);
				if (listener != null) {
					if (listener instanceof CompleteListener)
						((CompleteListener) listener).onResponse(new ApiResponse(response));

				}
			}
		};
		ErrorListener errorListener = new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				XLog.d(TAG, "volley error: " + error.getClass());
				if (listener != null) {
					String errorMessage = "";
					int code = ApiResponse.ERROR_CODE_UNKOWN;
					if (error.getClass() == TimeoutError.class) {
						code = ApiResponse.ERROR_CODE_REQUEST_TIMEOUT;
						errorMessage = "Request timeout";
					} else if (error.getClass() == com.android.volley.AuthFailureError.class) {
						errorMessage = error.getMessage() != null ? error.getMessage() : "No internet permission ?";
					} else {
						errorMessage = error.getMessage();
						if (errorMessage == null || errorMessage.length() == 0)
							errorMessage = error.toString();
						code = error.networkResponse != null ? error.networkResponse.statusCode
								: ApiResponse.ERROR_CODE_UNKOWN;
					}

					if (code == Constant.API_ERROR_CODE_UNAUTHORIZED) {
						// show login popup
					} else if (code == Constant.API_ERROR_CODE_SERVER_OFFLINE) {
						// show alert and turn off the app
					}

					if (listener instanceof CompleteListener)
						((CompleteListener) listener).onResponse(new ApiResponse(code, errorMessage));

				}
			}
		};

		XLog.d(TAG, "starting request url:" + url);
		ApiHelperJsonObjectRequest jr = new ApiHelperJsonObjectRequest(method, url, fullParams, completeListener,
				errorListener);
		if(tag != null){
			jr.setTag(tag);
		}
		RequestManager.getRequestQueue().add(jr);
	}

	public interface CompleteListener {
		public void onResponse(ApiResponse responseData);
	}

	/**
	 * A request for retrieving a {@link JSONObject} response body at a given
	 * URL. Use for posting params along with request, instead of posting body
	 * like {@link JsonObjectRequest}
	 */
	private class ApiHelperJsonObjectRequest extends JsonObjectRequest {
		Map<String, String> mParams;

		public ApiHelperJsonObjectRequest(int method, String url, Map<String, String> params,
				Listener<JSONObject> listener, ErrorListener errorListener) {
			super(method, url, null, listener, errorListener);
			mParams = params;
		}

		@Override
		public Map<String, String> getHeaders() throws AuthFailureError {
			HashMap<String, String> params = new HashMap<String, String>();
			params.putAll(super.getHeaders());
			params.put("apikey", new String(Constant.API_KEY));
			params.put("sid", App.getInstance().getSessionId());
//			params.put("sgid", Constant.SINGER_ID + "");
			XLog.d(TAG, "header params:" + params.toString());
			return params;
		}

		// override getBodyContentType and getBody for prevent posting body.
		@Override
		public String getBodyContentType() {
			return null;
		}

		@Override
		public byte[] getBody() {
			if (this.getMethod() == METHOD_POST && mParams != null && mParams.size() > 0) {
				return encodeParameters(mParams, getParamsEncoding());
			}
			return null;
		}

		/**
		 * Converts <code>params</code> into an
		 * application/x-www-form-urlencoded encoded string.
		 */
		private byte[] encodeParameters(Map<String, String> params, String paramsEncoding) {
			StringBuilder encodedParams = new StringBuilder();
			try {
				for (Map.Entry<String, String> entry : params.entrySet()) {
					encodedParams.append(URLEncoder.encode(entry.getKey(), paramsEncoding));
					encodedParams.append('=');
					encodedParams.append(URLEncoder.encode(entry.getValue(), paramsEncoding));
					encodedParams.append('&');
				}
				return encodedParams.toString().getBytes(paramsEncoding);
			} catch (UnsupportedEncodingException uee) {
				throw new RuntimeException("Encoding not supported: " + paramsEncoding, uee);
			}

		}

	}

//	public void multipartRequest(final String url, HashMap<String, String> params, final HashMap<String, File> files,
//			final CompleteListener listener) {
//
//		final HashMap<String, String> fullParams = new HashMap<String, String>();
//		fullParams.putAll(this.defaultParams);
//		if (params != null) {
//			fullParams.putAll(params);
//		}
//
//		XLog.d(TAG, "multipartRequest params:" + fullParams.toString());
//		XLog.d(TAG, "multipartRequest files:" + files.keySet().toString());
//
//		Thread thread = new Thread(new Runnable() {
//			public void run() {
//				String response_str = null;
//				try {
//					HttpParams httpParameters = new BasicHttpParams();
//					HttpProtocolParams.setContentCharset(httpParameters, HTTP.UTF_8);
//					HttpProtocolParams.setHttpElementCharset(httpParameters, HTTP.UTF_8);
//
//					HttpClient client = new DefaultHttpClient(httpParameters);
//					client.getParams().setParameter("http.protocol.version", HttpVersion.HTTP_1_1);
//					client.getParams().setParameter("http.socket.timeout", 10000);
//					client.getParams().setParameter("http.protocol.content-charset", HTTP.UTF_8);
//					HttpPost post = new HttpPost(url);
//					MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.STRICT, null,
//							Charset.forName("UTF-8"));
//
//					for (Map.Entry<String, File> file : files.entrySet()) {
//						String mime = FileHelper.getMimeType(file.getValue().getPath());
//						XLog.i(TAG, "Uploading file: " + file.getValue().getPath() + " mime: " + mime
//								+ " is file exit =" + file.getValue().exists());
//						FileBody bin = new FileBody(file.getValue(), mime);
//						reqEntity.addPart(file.getKey(), bin);
//					}
//
//					for (Map.Entry<String, String> param : fullParams.entrySet()) {
//						reqEntity.addPart(param.getKey(), new StringBody(param.getValue()));
//					}
////					post.setHeader("sid", App.getInstance().getSessionId());
//					post.setHeader("apikey", new String(Constant.API_KEY));
////					post.setHeader("sgid", Constant.SINGER_ID + "");
////					post.setEntity(reqEntity);
//					HttpResponse response = client.execute(post);
//
//					response_str = EntityUtils.toString(response.getEntity());
//					XLog.d(TAG, "multipartRequest response:" + response_str);
//				} catch (Exception e) {
//					XLog.e(TAG, "multipartRequest error: " + e.getMessage());
//				}
//
//				if (listener != null) {
//					String errorMessage = "";
//					int code = 0;
//					JSONObject jo = null;
//
//					if (response_str == null) {
//						code = ApiResponse.ERROR_CODE_INVALID_RESPONSE_FORMAT;
//						errorMessage = "null response";
//					} else {
//						try {
//							jo = new JSONObject(response_str);
//						} catch (JSONException e) {
//							code = ApiResponse.ERROR_CODE_INVALID_RESPONSE_FORMAT;
//							errorMessage = "Invalid json format";
//						}
//					}
//
//					if (jo != null && jo.optJSONObject("error") != null) {
//						JSONObject errorObj = jo.optJSONObject("error");
//						code = errorObj.optInt("code", ApiResponse.ERROR_CODE_UNKOWN);
//						errorMessage = errorObj.optString("message");
//					}
//
//					if (code == Constant.API_ERROR_CODE_UNAUTHORIZED) {
//						// show login popup
//					} else if (code == Constant.API_ERROR_CODE_SERVER_OFFLINE) {
//						// show alert and turn off the app
//					} else if (listener instanceof CompleteListener)
//						((CompleteListener) listener).onResponse(new ApiResponse(jo));
//
//				}
//			}
//		});
//		thread.start();
//
//	}
//
//	public void multipartRequestWithProgress(final String url, HashMap<String, String> params,
//			final HashMap<String, File> files, final UploadProgressListener listener, final int totalProgress) {
//
//		final int numTotal = 5;
//
//		final HashMap<String, String> fullParams = new HashMap<String, String>();
//		fullParams.putAll(this.defaultParams);
//		if (params != null) {
//			fullParams.putAll(params);
//		}
//
//		XLog.d(TAG, "multipartRequest params:" + fullParams.toString());
//		XLog.d(TAG, "multipartRequest files:" + files.keySet().toString());
//
//		listener.add(totalProgress / numTotal);
//
//		Thread thread = new Thread(new Runnable() {
//			public void run() {
//				String response_str = null;
//				try {
//					HttpParams httpParameters = new BasicHttpParams();
//					HttpProtocolParams.setContentCharset(httpParameters, HTTP.UTF_8);
//					HttpProtocolParams.setHttpElementCharset(httpParameters, HTTP.UTF_8);
//
//					HttpClient client = new DefaultHttpClient(httpParameters);
//					client.getParams().setParameter("http.protocol.version", HttpVersion.HTTP_1_1);
//					client.getParams().setParameter("http.socket.timeout", 10000);
//					client.getParams().setParameter("http.protocol.content-charset", HTTP.UTF_8);
//					HttpPost post = new HttpPost(url);
//					CountingMultiPartEntity reqEntity = new CountingMultiPartEntity(HttpMultipartMode.STRICT, null,
//							Charset.forName("UTF-8"));
//					reqEntity.setListener(listener);
//					listener.add(totalProgress / numTotal);
//
//					for (Map.Entry<String, File> file : files.entrySet()) {
//						String mime = FileHelper.getMimeType(file.getValue().getPath());
//						XLog.i(TAG, "Uploading file: " + file.getValue().getPath() + " mime: " + mime
//								+ " is file exit =" + file.getValue().exists());
//						FileBody bin = new FileBody(file.getValue(), mime);
//						reqEntity.addPart(file.getKey(), bin);
//					}
//
//					listener.add(totalProgress / numTotal);
//
//					for (Map.Entry<String, String> param : fullParams.entrySet()) {
//						reqEntity.addPart(param.getKey(), new StringBody(param.getValue()));
//					}
//
//					listener.add(totalProgress / numTotal);
//
////					post.setHeader("sid", App.getInstance().getSessionId());
//					post.setHeader("apikey", new String(Constant.API_KEY));
////					post.setHeader("sgid", Constant.SINGER_ID + "");
//					post.setEntity(reqEntity);
//					HttpResponse response = client.execute(post);
//
//					response_str = EntityUtils.toString(response.getEntity());
//					XLog.d(TAG, "multipartRequest response:" + response_str);
//				} catch (Exception e) {
//					XLog.e(TAG, "multipartRequest error: " + e.getMessage());
//				}
//
//				listener.add(totalProgress / numTotal);
//
//				if (listener != null) {
//					String errorMessage = "";
//					int code = 0;
//					JSONObject jo = null;
//
//					if (response_str == null) {
//						code = ApiResponse.ERROR_CODE_INVALID_RESPONSE_FORMAT;
//						errorMessage = "null response";
//					} else {
//						try {
//							jo = new JSONObject(response_str);
//						} catch (JSONException e) {
//							code = ApiResponse.ERROR_CODE_INVALID_RESPONSE_FORMAT;
//							errorMessage = "Invalid json format";
//						}
//					}
//
//					if (jo != null && jo.optJSONObject("error") != null) {
//						JSONObject errorObj = jo.optJSONObject("error");
//						code = errorObj.optInt("code", ApiResponse.ERROR_CODE_UNKOWN);
//						errorMessage = errorObj.optString("message");
//					}
//
//					if (code == Constant.API_ERROR_CODE_UNAUTHORIZED) {
//						// show login popup
//					} else if (code == Constant.API_ERROR_CODE_SERVER_OFFLINE) {
//						// show alert and turn off the app
//					} else if (listener instanceof UploadProgressListener)
//						((UploadProgressListener) listener).onResponse(new ApiResponse(jo));
//
//				}
//			}
//		});
//		thread.start();
//
//	}

	public String sendFileToServer(String filename, String targetUrl, HashMap<String, String> params) {
		String response = "error";
		Log.e("Image filename", filename);
		Log.e("url", targetUrl);
		HttpURLConnection connection = null;
		DataOutputStream outputStream = null;
		// DataInputStream inputStream = null;

		String pathToOurFile = filename;
		String urlServer = targetUrl;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		SimpleDateFormat df = new SimpleDateFormat("yyyy_MM_dd_HH:mm:ss");

		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024;
		try {
			FileInputStream fileInputStream = new FileInputStream(new File(pathToOurFile));

			URL url = new URL(urlServer);
			connection = (HttpURLConnection) url.openConnection();

			// Allow Inputs & Outputs
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setChunkedStreamingMode(1024);
			// Enable POST method
			connection.setRequestMethod("POST");

			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
			connection.setRequestProperty("sid", App.getInstance().getSessionId());
//			connection.setRequestProperty("sgid", Constant.SINGER_ID + "");
			connection.setRequestProperty("apikey", new String(Constant.API_KEY));

			outputStream = new DataOutputStream(connection.getOutputStream());
			
//			outputStream.writeBytes(twoHyphens + boundary + lineEnd);
			outputStream.writeBytes(getPostParamString(params));
			
			String connstr = null;
			connstr = "Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + pathToOurFile + "\""
					+ lineEnd;
			Log.i("Connstr", connstr);

			outputStream.writeBytes(connstr);
			outputStream.writeBytes(lineEnd);

			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// Read file
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			Log.e("Image length", bytesAvailable + "");
			try {
				while (bytesRead > 0) {
					try {
						outputStream.write(buffer, 0, bufferSize);
					} catch (OutOfMemoryError e) {
						e.printStackTrace();
						response = "outofmemoryerror";
						return response;
					}
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				}
			} catch (Exception e) {
				e.printStackTrace();
				response = "error";
				return response;
			}
			outputStream.writeBytes(lineEnd);
			outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// Responses from the server (code and message)
			int serverResponseCode = connection.getResponseCode();
			String serverResponseMessage = connection.getResponseMessage();
			Log.i("Server Response Code ", "" + serverResponseCode);
			Log.i("Server Response Message", serverResponseMessage);

			if (serverResponseCode == 200) {
				response = "true";
			}

			String CDate = null;
			Date serverTime = new Date(connection.getDate());
			try {
				CDate = df.format(serverTime);
			} catch (Exception e) {
				e.printStackTrace();
				Log.e("Date Exception", e.getMessage() + " Parse Exception");
			}
			Log.i("Server Response Time", CDate + "");

			filename = CDate + filename.substring(filename.lastIndexOf("."), filename.length());
			Log.i("File Name in Server : ", filename);

			fileInputStream.close();
			outputStream.flush();
			outputStream.close();
			outputStream = null;
		} catch (Exception ex) {
			// Exception handling
			response = "error";
			Log.e("Send file Exception", ex.getMessage() + "");
			ex.printStackTrace();
		}
		return response;
	}

	private static String getPostParamString(HashMap<String, String> params) {
		if (params.size() == 0)
			return "";

		StringBuffer buf = new StringBuffer();
		for (Map.Entry<String, String> param : params.entrySet()) {
			buf.append(buf.length() == 0 ? "" : "&");
			String key = param.getKey();
			buf.append(key).append("=").append(params.get(key));
		}

		// Enumeration<String> keys = params.keys();
		// while(keys.hasMoreElements()) {
		// buf.append(buf.length() == 0 ? "" : "&");
		// String key = keys.nextElement();
		// buf.append(key).append("=").append(params.get(key));
		// }
		return buf.toString();
	}
}
