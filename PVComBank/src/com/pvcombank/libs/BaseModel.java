package com.pvcombank.libs;

import java.util.Collection;
import java.util.HashMap;

import org.json.JSONObject;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class BaseModel {
	private HashMap<String, Object> extraData;

	public BaseModel(JSONObject jo) {

	}

	public BaseModel() {

	}

	public String toJson() {
		Gson gson = new Gson();
		String json = gson.toJson(this);
		return json;
	}

	public static Gson newGson() {
		return new GsonBuilder()
				.setFieldNamingPolicy(
						FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
				// .registerTypeAdapterFactory(new TypeAdapterFactory())
				.registerTypeAdapter(boolean.class, new BooleanSerializer())
				.registerTypeAdapter(Collection.class,
						new CollectionDeserializer()).create();
	}

	public HashMap<String, Object> getExtraData() {
		if (extraData == null)
			extraData = new HashMap<String, Object>();
		return extraData;
	}

	public void setExtraData(HashMap<String, Object> extraData) {
		this.extraData = extraData;
	}
}
