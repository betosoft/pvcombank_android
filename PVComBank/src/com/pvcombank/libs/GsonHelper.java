package com.pvcombank.libs;

import java.util.Collection;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonHelper {

	public static Gson getGsonSetting() {
		return new GsonBuilder()
				.setFieldNamingPolicy(
						FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
				// .registerTypeAdapterFactory(new TypeAdapterFactory())
				.registerTypeAdapter(boolean.class, new BooleanSerializer())
				.registerTypeAdapter(Collection.class,
						new CollectionDeserializer()).create();
	}
}
