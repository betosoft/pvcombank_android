package com.pvcombank.libs;

import java.util.HashMap;

public class ApiParams extends HashMap<String, String>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 122222333333333L;

	public ApiParams putParam(String key, Object value){
		this.put(key, value.toString());
		return this;
	}
	public HashMap<String, String> build(){
		return (HashMap<String, String>)this;
	}
	
	public static ApiParams getBuilder(){
		return new ApiParams();
	}
	
}
