package com.pvcombank.libs;

import java.io.File;
import java.util.UUID;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

public class DeviceInfo {
	private static final String TAG = "DeviceInfo";

	public static String getDeviceModel() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return StringHelper.Capitalize(model);
		} else {
			return StringHelper.Capitalize(manufacturer) + " " + model;
		}
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static Point getScreenDemension(Activity ctx){
		Display display = ctx.getWindowManager().getDefaultDisplay();
		Point size = new Point();
		
		if(android.os.Build.VERSION.SDK_INT >= 13){
			display.getSize(size);
		} else {
			size.x = display.getWidth();
			size.y = display.getHeight();
		}
		
		return size;
	}
	
	public static String getOsVersion(){
		return Build.VERSION.RELEASE;
	}

	public static int getDensity(Context ctx){
		Display display = ((WindowManager) ctx
				.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		return metrics.densityDpi;
	}
	
	public static String getDeviceIdentity(Context context){
		String deviceId = DataStore.getInstance().getString("deviceid", "");

		if (deviceId.length() == 0) {
			XLog.d(TAG, "No deviceId.");

			// android ID
			deviceId = getAndroidID(context);
			// android 2.2 bug
			if (deviceId == "9774d56d682e549c")
				deviceId = null;

			// imei
			try {
				if (deviceId == null) {
					String imie = getImei(context);
					if (imie != null && imie.length() > 0)
						deviceId = getImei(context);
					else
						deviceId = null;
				}
			} catch (Exception e1) {
				XLog.e(TAG, "Can not get imei.");
			}

			// wifi mac address
			try {
				if (deviceId == null) {
					String mac = getMacAddress(context);
					if (mac != null && mac.length() > 0)
						deviceId = StringHelper.md5(getMacAddress(context));
					else
						deviceId = null;
				}
			} catch (Exception e) {
				XLog.e(TAG, "Can not get mac address.");
			}

			// not at all
			if (deviceId == null) {
				UUID deviceUuid = UUID.randomUUID();
				deviceId = deviceUuid.toString();
			}

			// save to local
			DataStore.getInstance()
				.saveString("deviceid", deviceId)
				.commit();
		}

		return deviceId;
	}

	public static String getImei(Context context) {
		TelephonyManager mTelephonyMgr;
		mTelephonyMgr = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		return mTelephonyMgr.getDeviceId();
	}

	public static String getMacAddress(Context context) {
		WifiManager wm = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);
		return wm.getConnectionInfo().getMacAddress();
	}
	
	public static String getAndroidID(Context mAppContext) {
		String androidId = android.provider.Settings.Secure.getString(
				mAppContext.getContentResolver(),
				android.provider.Settings.Secure.ANDROID_ID);
		return androidId;
	}
	
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	public static boolean isExternalStorageRemovable() {
		if (hasGingerBread()) {
			return Environment.isExternalStorageRemovable();
		}
		return true;
	}

	public static File getExternalCacheDir(Context context) {
		if (hasFroyo()) {
			return context.getExternalCacheDir();
		}
		// Before Froyo we need to construct the external cache dir ourselves
		final String cacheDir = "/Android/data/" + context.getPackageName()
				+ "/cache/";
		return new File(Environment.getExternalStorageDirectory().getPath()
				+ cacheDir);
	}

	// Android 2.2
	public static boolean hasFroyo() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
	}

	// Android 2.3
	public static boolean hasGingerBread() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
	}

	// Android 2.3
	public static boolean hasGingerBreadMR1() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD_MR1;
	}

	// Android 3.0
	public static boolean hasHoneycomb() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
	}

	// Android 3.1
	public static boolean hasHoneycombMR1() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
	}

	// Android 3.2
	public static boolean hasHoneycombMR2() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2;
	}

	// Android 4.0
	public static boolean hasICS() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
	}

	// Android 4.0.3
	public static boolean hasICSMR1() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1;
	}

	// Android 4.1
	public static boolean hasJellyBean() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
	}

	// Android 4.2
	public static boolean hasJellyBeanMR1() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1;
	}

	public static String getOsName() {
		if (Build.VERSION.SDK_INT >= 16) { // Build.VERSION_CODES.JELLY_BEAN
			return "Jelly bean";
		} else if (Build.VERSION.SDK_INT >= 14) { // Build.VERSION_CODES.ICE_CREAM_SANDWICH
			return "Ice cream sandwich";
		} else if (Build.VERSION.SDK_INT >= 11) { // Build.VERSION_CODES.HONEYCOMB
			return "Honeycomb";
		} else if (Build.VERSION.SDK_INT >= 9) { // Build.VERSION_CODES.GINGERBREAD
			return "Gingerbread";
		} else if (Build.VERSION.SDK_INT >= 8) { // Build.VERSION_CODES.FROYO
			return "Froyo";
		} else {
			return "Eclair";
		}
	}

	public static String getDensityType(int densityDpi) {
		if (densityDpi >= 480) {
			return "xxhigh";
		} else if (densityDpi >= 320) {
			return "xhigh";
		} else if (densityDpi >= 240) {
			return "high";
		} else if (densityDpi >= 160) {
			return "medium";
		} else {
			return "low";
		}
	}
}
