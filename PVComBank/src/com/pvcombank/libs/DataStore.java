package com.pvcombank.libs;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class DataStore {
	public static final String PREFS_NAME = "prefs";

	private static SharedPreferences sharedPrefs;
	private Editor prefsEditor;
	private static Context context;
	private static DataStore instance = null;
	
	public static DataStore getInstance () {
		if (instance == null)
		{
			throw new RuntimeException ("DataStore has not initialized!");
		}
		return instance;
	}
	
	public static void init(Context ctx) {
		if (instance == null)
		{
			instance = new DataStore();
			context = ctx;
			sharedPrefs = context.getSharedPreferences(PREFS_NAME,
					Context.MODE_PRIVATE);
		}
	}

	public DataStore saveString(String key, String content) {
		if (prefsEditor == null) prefsEditor = sharedPrefs.edit();
		prefsEditor.putString(key, content);
		return this;
	}

	public DataStore saveInt(String key, int value) {
		if (prefsEditor == null) prefsEditor = sharedPrefs.edit();
		prefsEditor.putInt(key, value);
		return this;
	}

	public DataStore saveLong(String key, long value) {
		if (prefsEditor == null) prefsEditor = sharedPrefs.edit();
		prefsEditor.putLong(key, value);
		return this;
	}

	public String getString(String key) {
		return getString(key, "");
	}

	public String getString(String key, String defaultValue) {
		return sharedPrefs.getString(key, defaultValue);
	}

	public int getInt(String key) {
		return getInt(key, 0);
	}

	public int getInt(String key, int defaultValue) {
		return sharedPrefs.getInt(key, defaultValue);
	}

	public long getLong(String key) {
		return getLong(key, 0);
	}
	
	public long getLong(String key, long defaultValue) {
		return sharedPrefs.getLong(key, defaultValue);
	}
	
	public DataStore removeKey (String key) {
		if (prefsEditor == null)
			throw new RuntimeException("Pref Editor not started");
		prefsEditor.remove(key); 
		return this;
	}


	public DataStore startEdit() {
		if (prefsEditor != null) {
			prefsEditor.commit();
		}
		prefsEditor = sharedPrefs.edit();
		return this;
	}

	public DataStore commit() {
		if (prefsEditor == null)
			throw new RuntimeException("Pref Editor not started");
		prefsEditor.commit();
		return this;
	}
}
