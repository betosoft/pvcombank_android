package com.pvcombank.libs;

import android.webkit.MimeTypeMap;

public final class FileHelper {
	public static String getMimeType(String url)
	{
	    String type = null;
	    String extension = getFileExtension(url);
	    if (extension != null) {
	        MimeTypeMap mime = MimeTypeMap.getSingleton();
	        type = mime.getMimeTypeFromExtension(extension);
	    }
	    return type;
	}
	
	public static String getFileExtension(String path){
		String filenameArray[] = path.split("\\.");
		return filenameArray[filenameArray.length-1];
	}
	
	public static String getFileNameWithoutExtension(String path){
		String fileName = path.substring(path.lastIndexOf("/")+1);
		int pos = fileName.lastIndexOf(".");
		if (pos > 0) {
			fileName = fileName.substring(0, pos);
		}
		return fileName;
	}
}

