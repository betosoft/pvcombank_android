package com.pvcombank.libs;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Build;
import android.support.v4.util.LruCache;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageCache;
import com.android.volley.toolbox.ImageLoader.ImageListener;

/**
 * Implementation of volley's ImageCache interface. This manager tracks the
 * application image loader and cache.
 * 
 * @credit Trey Robinson
 * @author HungLV
 * 
 */
public class ImageCacheManager implements ImageCache {
	private static final String TAG = ImageCacheManager.class.getSimpleName();
	private static ImageCacheManager mInstance;
	private static int DISK_IMAGECACHE_SIZE = 1024 * 1024 * 50; // 50 MB
	private static CompressFormat DISK_IMAGECACHE_COMPRESS_FORMAT = CompressFormat.PNG;
	private static int DISK_IMAGECACHE_QUALITY = 100; // PNG is lossless so
														// quality is ignored
														// but must be provided

	/**
	 * Volley image loader
	 */
	private ImageLoader mImageLoader;

	/**
	 * Image cache used for local image storage
	 */
	private DiskLruImageCache mDiskCache;
	private LruCache<String, Bitmap> mMemoryCache;
	
	private ImageCacheManager(){
		
	}

	/**
	 * @return instance of the cache manager
	 */
	public static ImageCacheManager getInstance() {
		if (mInstance != null) {
	        return mInstance;
	    } else {
	        throw new IllegalStateException("Not initialized");
	    }
	}

	/**
	 * Initializer for the manager. Must be called prior to use. Using default
	 * options: uniqueName: imagecache; cacheSize: 50 MB; compressFormat: PNG;
	 * quality: 100
	 * 
	 * @param context
	 *            application context
	 */
	public static void init(Context context) {
		if (mInstance == null) {
			mInstance = new ImageCacheManager();
			mInstance.prepare(context, null, DISK_IMAGECACHE_SIZE,
					DISK_IMAGECACHE_COMPRESS_FORMAT, DISK_IMAGECACHE_QUALITY);
		}
	}

	/**
	 * Initializer for the manager. Must be called prior to use.
	 * 
	 * @param context
	 *            application context
	 * @param uniqueName
	 *            name for the cache location. default: imagecache
	 * @param cacheSize
	 *            max size for the cache
	 * @param compressFormat
	 *            file type compression format.
	 * @param quality
	 */
	public static void init(Context context, String uniqueName, int cacheSize,
			CompressFormat compressFormat, int quality) {
		if (mInstance == null) {
			mInstance = new ImageCacheManager();
			mInstance.prepare(context, uniqueName, cacheSize, compressFormat,
					quality);
		}
	}

	public void prepare(Context context, String uniqueName, int cacheSize,
			CompressFormat compressFormat, int quality) {
		// init disk cache
		if (uniqueName == null)
			uniqueName = "imagecache";
		mDiskCache = new DiskLruImageCache(context, uniqueName, cacheSize,
				compressFormat, quality);
		XLog.d(TAG, "diskcache init with size: " + cacheSize / 1024 / 1024
				+ " MB" + "\npath: "
				+ mDiskCache.getCacheFolder().getAbsolutePath());

		// init mem cache
		// Get max available VM memory, exceeding this amount will throw an
		// OutOfMemory exception. Stored in kilobytes as LruCache takes an
		// int in its constructor.
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		final int memCacheSize = maxMemory / 8;
		mMemoryCache = new LruCache<String, Bitmap>(memCacheSize) {
			@Override
			protected int sizeOf(String key, Bitmap bitmap) {
				return getBitmapByteCount(bitmap) / 1024;
			}
		};
		XLog.d(TAG, "memcache init with size: " + memCacheSize + " KB");

		// init image loader
		mImageLoader = new ImageLoader(RequestManager.getRequestQueue(), this);
		mImageLoader.setBatchedResponseDelay(0);
	}

	@Override
	public Bitmap getBitmap(String url) {
		// get from memory first
		Bitmap bitmap = this.getFromMemoryCache(url);
		if (bitmap == null) {
			bitmap = this.getFromDiskCache(url);
			if (bitmap != null) {
				putToMemoryCache(url, bitmap);
			}
		}

		return bitmap;
	}

	@Override
	public void putBitmap(String url, Bitmap bitmap) {
		if (url != null && bitmap != null) {
			putToMemoryCache(url, bitmap);
			putToDiskCache(url, bitmap);
		}
	}

	/**
	 * Executes and image load
	 * 
	 * @param url
	 *            location of image
	 * @param listener
	 *            Listener for completion
	 */
	public void getImage(String url, ImageListener listener) {
		mImageLoader.get(url, listener);
	}

	/**
	 * @return instance of the image loader
	 */
	public ImageLoader getImageLoader() {
		return mImageLoader;
	}

	/**
	 * Creates a unique cache key based on a url value
	 * 
	 * @param url
	 *            url to be used in key creation
	 * @return cache key value
	 */
	private String createKey(String url) {
		return String.valueOf(url.hashCode());
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
	private int getBitmapByteCount(Bitmap bitmap) {
		if (android.os.Build.VERSION.SDK_INT >= 12) {
			// android 3.0 and later
			return bitmap.getByteCount();
		}
		return bitmap.getRowBytes() * bitmap.getHeight();
	}

	public Bitmap getFromMemoryCache(String key) {
		Bitmap bitmap = null;
		String cacheKey = createKey(key);
		if (mMemoryCache != null && key != null) {
			bitmap = mMemoryCache.get(cacheKey);
		}
		return bitmap;
	}

	public Bitmap getFromDiskCache(String key) {
		Bitmap bitmap = null;
		String cacheKey = createKey(key);
		if (mDiskCache != null && key != null) {
			bitmap = mDiskCache.getBitmap(cacheKey);
		}
		return bitmap;
	}

	public void putToMemoryCache(String key, Bitmap bitmap) {
		key = createKey(key);
		if (mMemoryCache != null && mMemoryCache.get(key) == null) {
			mMemoryCache.put(key, bitmap);
		}
	}

	public void putToDiskCache(String key, Bitmap bitmap) {
		key = createKey(key);
		if (mDiskCache != null && mDiskCache.containsKey(key) == false) {
			mDiskCache.put(key, bitmap);
		}
	}
}
