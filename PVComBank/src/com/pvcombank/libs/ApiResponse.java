package com.pvcombank.libs;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ApiResponse {
	public static final String KEY_NAME_ERROR = "error";
	public static final String KEY_NAME_CODE = "code";
	public static final String KEY_NAME_MESSAGE = "message";
	public static final String KEY_NAME_DATA = "data";
	public static final String KEY_NAME_TOTAL = "total";
	public static final String KEY_NAME_UPDATELINK = "update_link";

	public static final int ERROR_CODE_NOERROR = 0;
	public static final int ERROR_CODE_UNKOWN = -9000;
	public static final int ERROR_CODE_JSON_ERROR = -9001;
	public static final int ERROR_CODE_INVALID_RESPONSE_FORMAT = -9002;
	public static final int ERROR_CODE_REQUEST_TIMEOUT = -9003;

	private int code;
	private boolean isError;
	private String message;
	private JSONObject data;
	private String updateLink;

	public ApiResponse(int code, String message) {
		this.setCode(code);
		this.message = message != null ? message : "";
		this.isError = code != ERROR_CODE_NOERROR;
	}

	public ApiResponse(JSONObject json) {
		// parse json result
		// if (json == null) {
		// this.isError = true;
		// this.message = "Empty json";
		// this.code = ERROR_CODE_JSON_ERROR;
		// } else {
		// if (json.has(KEY_NAME_ERROR)) {
		// this.isError = true;
		// JSONObject jo;
		// try {
		// jo = json.getJSONObject(KEY_NAME_ERROR);
		// this.code = jo.optInt(KEY_NAME_CODE, ERROR_CODE_JSON_ERROR);
		// this.message = jo.optString(KEY_NAME_MESSAGE, "Json error");
		// this.updateLink = jo.optString(KEY_NAME_UPDATELINK, "");
		// this.data = json;
		// } catch (JSONException e) {
		// e.printStackTrace();
		// }
		// } else {
		// if (json.has(KEY_NAME_MESSAGE)) {
		// this.message = json.optString(KEY_NAME_MESSAGE, "");
		// }
		// this.data = json;
		// }
		// }

		if (json == null) {
			this.isError = true;
			this.message = "Empty json";
			this.code = ERROR_CODE_JSON_ERROR;
		} else {
			if (json.has(KEY_NAME_ERROR)) {
				// this.code = json.optInt(KEY_NAME_ERROR,
				// ERROR_CODE_JSON_ERROR);
				// if(code == ERROR_CODE_NOERROR){
				// this.isError = false;
				// }
				// else{
				// this.isError = true;
				// }
				// this.message = json.optString(KEY_NAME_MESSAGE,
				// "Json error");
				// this.data = json;

				this.isError = true;
				JSONObject jo;
				try {
					jo = json.getJSONObject(KEY_NAME_ERROR);
					this.code = jo.optInt(KEY_NAME_CODE, ERROR_CODE_JSON_ERROR);
					this.message = jo.optString(KEY_NAME_MESSAGE, "Json error");
//					this.updateLink = jo.optString(KEY_NAME_UPDATELINK, "");
					this.data = json;
				} catch (JSONException e) {
				}
			} else {
				// {"message":"session timeout","error":402,"total":0,"data":[]}
				this.code = json.optInt(KEY_NAME_CODE, ERROR_CODE_JSON_ERROR);
				if (json.has(KEY_NAME_MESSAGE)) {
					this.message = json.optString(KEY_NAME_MESSAGE, "Json error");
				}
				// try {
				this.data = json; // .getJSONObject("data");
				// } catch (JSONException e) {
				// e.printStackTrace();
			}
		}
	}

	public List<JSONObject> getResponseObjects() {
		List<JSONObject> objects = new ArrayList<JSONObject>();
		JSONArray arr = this.data != null ? this.data.optJSONArray(KEY_NAME_DATA) : null;
		if (arr != null) {
			for (int i = 0; i < arr.length(); i++) {
				JSONObject jo = arr.optJSONObject(i);
				if (jo != null)
					objects.add(jo);
			}
		}

		return objects;
	}

	public List<JSONObject> getResponseObjects(String key) {
		List<JSONObject> objects = new ArrayList<JSONObject>();
		JSONArray arr = this.data != null ? this.data.optJSONArray(key) : null;
		if (arr != null) {
			for (int i = 0; i < arr.length(); i++) {
				JSONObject jo = arr.optJSONObject(i);
				if (jo != null)
					objects.add(jo);
			}
		}

		return objects;
	}

	public int getResponseTotalObject() {
		return this.data != null ? this.data.optInt(KEY_NAME_TOTAL, 0) : 0;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public boolean isError() {
		return this.isError;
	}

	public JSONObject getData() {
		return data;
	}

	public void setData(JSONObject data) {
		this.data = data;
	}

	public String getUpdateLink() {
		return updateLink;
	}

	public void setUpdateLink(String updateLink) {
		this.updateLink = updateLink;
	}

}
