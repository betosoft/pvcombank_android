package com.pvcombank.libs;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.annotation.SuppressLint;

public class StringHelper {
	public static String getHumanReadableDate(String date) {
		Date d = null;
		try {
			SimpleDateFormat spd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
					new Locale("en"));
			spd.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
			d = spd.parse(date);
		} catch (ParseException e) {
		}
		return getHumanReadableDate(d);
	}

	public static String getHumanReadableDate(Date date) {
		String humanReadableTime = "";

		if (date == null) {
			return humanReadableTime;
		}

		Calendar now = Calendar.getInstance(TimeZone
				.getTimeZone("Asia/Bangkok"));
		Calendar cal = Calendar.getInstance(TimeZone
				.getTimeZone("Asia/Bangkok"));
		cal.setTime(date);

		if (now.compareTo(cal) < 0
				|| now.get(Calendar.YEAR) != cal.get(Calendar.YEAR)) {
			SimpleDateFormat spd = new SimpleDateFormat(
					"dd 'Tháng' MM yyyy 'Lúc' HH:mm", new Locale("en"));
			spd.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
			humanReadableTime = spd.format(date);
		} else {
			float offset = (float) ((now.getTimeInMillis() / 1000) - (cal
					.getTimeInMillis() / 1000));

			int deltaS = (int) offset % 60;
			offset /= 60;
			int deltaM = (int) (offset % 60);
			offset /= 60;
			int deltaH = (int) (offset % 24);
			offset /= 24;
			int deltaD = (int) (offset > 1 ? Math.ceil((double) offset)
					: offset);

			if (deltaD > 1) {
				if (now.get(Calendar.DAY_OF_MONTH) == cal
						.get(Calendar.WEEK_OF_MONTH)) {
					int day = cal.get(Calendar.DAY_OF_WEEK);
					String strDay = "";
					switch (day) {
					case Calendar.MONDAY:
						strDay = "Thứ hai";
						break;
					case Calendar.TUESDAY:
						strDay = "Thứ ba";
						break;
					case Calendar.WEDNESDAY:
						strDay = "Thứ tư";
						break;
					case Calendar.THURSDAY:
						strDay = "Thứ năm";
						break;
					case Calendar.FRIDAY:
						strDay = "Thứ sáu";
						break;
					case Calendar.SATURDAY:
						strDay = "Thứ bảy";
						break;
					case Calendar.SUNDAY:
						strDay = "Chủ nhật";
						break;
					default:
						break;
					}

					SimpleDateFormat spd = new SimpleDateFormat("'" + strDay
							+ "' 'lúc' HH:mm", new Locale("en"));
					spd.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
					humanReadableTime = spd.format(date);
				} else {
					SimpleDateFormat spd = new SimpleDateFormat(
							"dd 'tháng' MM 'lúc' HH:mm", new Locale("en"));
					spd.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
					humanReadableTime = spd.format(date);
				}
			} else if (deltaD == 1) {
				SimpleDateFormat spd = new SimpleDateFormat(
						"'Hôm qua lúc' HH:mm", new Locale("en"));
				spd.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
				humanReadableTime = spd.format(date);
			} else if (deltaH == 1 && deltaD == 0) {
				humanReadableTime = "1 gi�? trước";
			} else if (deltaM == 1 && deltaH == 0 && deltaD == 0) {
				humanReadableTime = "1 phút trước";
			} else if (deltaH > 0 && deltaD == 0) {
				humanReadableTime = deltaH + " gi�? trước";
			} else if (deltaM > 0 && deltaH == 0 && deltaD == 0) {
				humanReadableTime = deltaM + " phút trước";
			} else if (deltaH == 0 && deltaD == 0 && deltaM == 0) {
				deltaS = deltaS < 1 ? 1 : deltaS;
				humanReadableTime = deltaS + " giây trước";
			}
		}

		return humanReadableTime;
	}

	public static String formatNumber(int num) {
		return num + "";
	}

	public static int getAgeFromDate(String date) {
		if (date == null || date.trim().length() < 1) {
			return 0;
		}

		Date d = null;
		try {
			SimpleDateFormat spd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
					new Locale("en"));
			spd.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
			d = spd.parse(date);

		} catch (ParseException e) {
		}

		return getAgeFromDate(d);
	}

	public static int getAgeFromDate(Date date) {
		if (date == null) {
			return 0;
		}

		Calendar today = Calendar.getInstance(TimeZone
				.getTimeZone("Asia/Bangkok"));
		Calendar dob = Calendar.getInstance(TimeZone
				.getTimeZone("Asia/Bangkok"));
		dob.setTime(date);
		int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
			age--;
		} else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
				&& today.get(Calendar.DAY_OF_MONTH) < dob
						.get(Calendar.DAY_OF_MONTH)) {
			age--;
		}

		return age < 0 ? 0 : age;
	}

	public static String Capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}

	public static String md5(String s) {
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest
					.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++)
				hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static boolean isBlank(String s) {
		return s == null || s.length() == 0 || isWhitespace(s);
	}

	public static boolean isNullOrEmpty(String s) {
		return s == null || s.length() == 0;
	}

	public static boolean isNullOrWhitespace(String s) {
		return s == null || isWhitespace(s);

	}

	private static boolean isWhitespace(String s) {
		int length = s.length();
		if (length > 0) {
			for (int i = 0; i < length; i++) {
				if (!Character.isWhitespace(s.charAt(i))) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	@SuppressLint("DefaultLocale")
	public static String convertMilisecondsToTimeString(int milliseconds) {
		int seconds = (int) ((milliseconds / 1000) % 60);
		int minutes = (int) ((milliseconds / 1000) / 60);
		String time = String.format("%02d:%02d", minutes, seconds);
		return time;
	}
}
