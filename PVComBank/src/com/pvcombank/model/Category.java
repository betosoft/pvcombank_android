package com.pvcombank.model;

import com.pvcombank.libs.BaseModel;

public class Category extends BaseModel {
	
	private int categoryId;
	private String title;
	private String image;

	
	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
