package com.pvcombank.model;

import java.util.ArrayList;

public class ListDeal {

	private ArrayList<Deal> data;

	/**
	 * @return the lstDeal
	 */
	public ArrayList<Deal> getData() {
		return data;
	}

	/**
	 * @param lstDeal the lstDeal to set
	 */
	public void setData(ArrayList<Deal> lstDeal) {
		this.data = lstDeal;
	}
}
