package com.pvcombank.model;

import com.pvcombank.libs.BaseModel;

public class Deal extends BaseModel {
	private int dealId;
	private String title;
	private String cover;
	private String image;
	private int categoryId;
	private String intro;
	private int viewCount;
	private int likeCount;
	private int voteCount;
	private int rate;
	private int isLiked;
	private int isRate;
	private String publishUp;
	private String publishDown;
	private int merchantId;
	private String merchantName;
	private int merchantPhone;
	private int merchantDaddress;
	private float merchantLong;
	private float merchantLat;

	public int getDealId() {
		return dealId;
	}

	public void setDealId(int dealId) {
		this.dealId = dealId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public int getViewCount() {
		return viewCount;
	}

	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}

	public int getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}

	public int getVoteCount() {
		return voteCount;
	}

	public void setVoteCount(int voteCount) {
		this.voteCount = voteCount;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public int getIsLiked() {
		return isLiked;
	}

	public void setIsLiked(int isLiked) {
		this.isLiked = isLiked;
	}

	public int getIsRate() {
		return isRate;
	}

	public void setIsRate(int isRate) {
		this.isRate = isRate;
	}

	public String getPublishUp() {
		return publishUp;
	}

	public void setPublishUp(String publishUp) {
		this.publishUp = publishUp;
	}

	public String getPublishDown() {
		return publishDown;
	}

	public void setPublishDown(String publishDown) {
		this.publishDown = publishDown;
	}

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public int getMerchantPhone() {
		return merchantPhone;
	}

	public void setMerchantPhone(int merchantPhone) {
		this.merchantPhone = merchantPhone;
	}

	public int getMerchantDaddress() {
		return merchantDaddress;
	}

	public void setMerchantDaddress(int merchantDaddress) {
		this.merchantDaddress = merchantDaddress;
	}

	public float getMerchantLong() {
		return merchantLong;
	}

	public void setMerchantLong(float merchantLong) {
		this.merchantLong = merchantLong;
	}

	public float getMerchantLat() {
		return merchantLat;
	}

	public void setMerchantLat(float merchantLat) {
		this.merchantLat = merchantLat;
	}

}
