package com.pvcombank.model;

import java.util.ArrayList;

public class ListCategory {

	private ArrayList<Category> data;

	/**
	 * @return the data
	 */
	public ArrayList<Category> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(ArrayList<Category> data) {
		this.data = data;
	}

	
	
}
