package com.pvcombank.model;

public class AppConfig {

	private int versionCode;
	private	String versionName;
	private	int irv;
	private	String wapUrl;
	private	String helpUrl;
	private	String tosUrl;
	private int update;
	private String updateMessage;
	private String updateLink;
	
	public int getUpdate() {
		return update;
	}

	public void setUpdate(int update) {
		this.update = update;
	}

	public String getUpdateMessage() {
		return updateMessage;
	}

	public void setUpdateMessage(String updateMessage) {
		this.updateMessage = updateMessage;
	}

	public String getUpdateLink() {
		return updateLink;
	}

	public void setUpdateLink(String updateLink) {
		this.updateLink = updateLink;
	}

	public int getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(int versionCode) {
		this.versionCode = versionCode;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public int getIrv() {
		return irv;
	}

	public void setIrv(int irv) {
		this.irv = irv;
	}

	public String getWapUrl() {
		return wapUrl;
	}

	public void setWapUrl(String wapUrl) {
		this.wapUrl = wapUrl;
	}

	public String getHelpUrl() {
		return helpUrl;
	}

	public void setHelpUrl(String helpUrl) {
		this.helpUrl = helpUrl;
	}

	public String getTosUrl() {
		return tosUrl;
	}

	public void setTosUrl(String tosUrl) {
		this.tosUrl = tosUrl;
	}

}
