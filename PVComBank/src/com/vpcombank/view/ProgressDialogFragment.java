package com.vpcombank.view;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;

public class ProgressDialogFragment extends DialogFragment {
	String message;

	public static ProgressDialogFragment newInstance() {
		return newInstance(null);
	}
	
	public static ProgressDialogFragment newInstance(String message) {
		ProgressDialogFragment frag = new ProgressDialogFragment();
		frag.message = message;
		return frag;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		final ProgressDialog dialog = new ProgressDialog(getActivity());
		
		if(savedInstanceState != null && savedInstanceState.containsKey("message")){
			message = savedInstanceState.getString("message");
		}
		
		if(message != null){
			dialog.setMessage(message);
		}
		dialog.setIndeterminate(true);
		dialog.setCancelable(false);

		// Disable the back button
		OnKeyListener keyListener = new OnKeyListener() {

			@Override
			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {
				return keyCode == KeyEvent.KEYCODE_BACK;
			}

		};
		dialog.setOnKeyListener(keyListener);
		return dialog;
	}
	
	
	@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("message", message);
    }

}