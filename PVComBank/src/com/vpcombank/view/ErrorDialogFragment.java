package com.vpcombank.view;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.pvcombank.pvcombank.R;

public class ErrorDialogFragment extends DialogFragment {
    public static ErrorDialogFragment newInstance(String message) {
    	ErrorDialogFragment frag = new ErrorDialogFragment();
         Bundle args = new Bundle();
         args.putString("message", message);
         frag.setArguments(args);
         return frag;
    }
    
    TextView lblMessage;
    Button btnClose;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	
    	getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.fragment_dialog_alert, container, false);
        lblMessage = (TextView) v.findViewById(R.id.lbl_alert_dialog_text);
        btnClose = (Button) v.findViewById(R.id.btn_alert_dialog_close);
        lblMessage.setText(getArguments().getString("message"));
        btnClose.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ErrorDialogFragment.this.dismiss();
			}
		});
        return v;
    }
    
    public ErrorDialogFragment setMessage(String message){
    	lblMessage.setText(message);
    	getArguments().putString("message", message);
    	return this;
    }

    public void show(FragmentManager manager) {
    	super.show(manager, "error");
    }
}

