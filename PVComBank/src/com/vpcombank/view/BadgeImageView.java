package com.vpcombank.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;
import android.widget.ImageView;

public class BadgeImageView extends ImageButton {

	public BadgeImageView(final Context context) {
		super(context);
	}

	public BadgeImageView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
	}

	public BadgeImageView(final Context context, final AttributeSet attrs, final int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
		final int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
		setMeasuredDimension(width, width * 21 / 18);
	}

	@Override
	protected void onSizeChanged(final int w, final int h, final int oldw, final int oldh) {
		super.onSizeChanged(w, w * 21 / 18, oldw, oldh);
	}
}
