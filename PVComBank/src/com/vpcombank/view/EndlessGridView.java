package com.vpcombank.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;

import com.pvcombank.libs.XLog;

public class EndlessGridView extends GridView implements OnScrollListener {
	private static final String TAG = EndlessGridView.class.getSimpleName();

	private LayoutInflater inflater;

	// private RelativeLayout footerView;
	private ProgressBar progressBarLoading;

	private OnScrollListener onScrollListener;
	private OnLoadMoreListener onLoadMoreListener;
	private boolean ended;

	private boolean isLoading = false;
	private int currentScrollState;

	public EndlessGridView(Context context) {
		super(context);
		init(context);
	}

	public EndlessGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public EndlessGridView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	private void init(Context context) {

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// footerView = (RelativeLayout)
		// inflater.inflate(R.layout.listitem_load_more, this, false);
		// progressBarLoading = (ProgressBar)
		// footerView.findViewById(R.id.listitem_load_more_progressbar);

		// addFooterView(footerView);
		// this.setFooterDividersEnabled(false);

		super.setOnScrollListener(this);
	}

	public void showProgressBar(boolean show) {
		progressBarLoading.setVisibility(show ? View.VISIBLE : View.GONE);
	}

	@Override
	public void setAdapter(ListAdapter adapter) {
		super.setAdapter(adapter);
	}

	/**
	 * Set the listener that will receive notifications every time the list
	 * scrolls.
	 * 
	 * @param l
	 *            The scroll listener.
	 */
	@Override
	public void setOnScrollListener(AbsListView.OnScrollListener listener) {
		onScrollListener = listener;
	}

	/**
	 * Register a callback to be invoked when this list reaches the end (last
	 * item be visible)
	 * 
	 * @param onLoadMoreListener
	 *            The callback to run.
	 */

	public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
		this.onLoadMoreListener = onLoadMoreListener;
	}

	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		// ZLog.d(TAG, "on scroll");
		if (onScrollListener != null) {
			onScrollListener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
		}

		if (onLoadMoreListener != null && !this.ended) { 
//			if (!isLoading) {
//				showViews(false);
//				return;
//			}

			boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
			if (!isLoading && loadMore && currentScrollState != SCROLL_STATE_IDLE) {
				onLoadMore();
			}

		}

	}

	public void onScrollStateChanged(AbsListView view, int scrollState) {
		currentScrollState = scrollState;

		if (onScrollListener != null) {
			onScrollListener.onScrollStateChanged(view, scrollState);
		}

	}

	public void onLoadMore() {
		XLog.d(TAG, "onLoadMore");
		showViews(true);
		isLoading = true;
		if (onLoadMoreListener != null) {
			onLoadMoreListener.onLoadMore();
		}
	}

	/**
	 * Notify the loading more operation has finished
	 */
	public void onLoadMoreComplete() {
		isLoading = false;
		showViews(false);
	}

	/**
	 * Interface definition for a callback to be invoked when list reaches the
	 * last item (the user load more items in the list)
	 */
	public interface OnLoadMoreListener {
		/**
		 * Called when the list reaches the last item (the last item is visible
		 * to the user)
		 */
		public void onLoadMore();
	}

	private void showViews(boolean show) {
		// if (show) {
		// footerView.setVisibility(View.VISIBLE);
		// } else {
		// footerView.setVisibility(View.GONE);
		// }
	}

	public boolean isEnded() {
		return ended;
	}

	public void setEnded(boolean ended) {
		this.ended = ended;
	}

}
