package com.vpcombank.facebook;


import com.facebook.Session;
import com.facebook.SessionState;

public interface FacebookStatusCallback {

	/**
	 * 
	 * @param session
	 * @param state
	 * @param exception
	 */
	public void facebookStatusCallback(Session session, SessionState state, Exception exception);
}
